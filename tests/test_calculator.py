import pytest
import sys

sys.path.append("src")
from calculator import Calculator


def test_add():
    res = Calculator.add(1, 2)
    excpected = 3
    assert  res == excpected, f"1 + 2 should equal {excpected} not {res}"
    res = Calculator.add(-1, 1)
    excpected = 0
    assert  res == excpected, f"-1 + 1 should equal {excpected} not {res}"


def test_sub():
    res = Calculator.sub(1, 2)
    excpected = -1
    assert  res == excpected, f"1 - 2 should equal {excpected} not {res}"
    res = Calculator.sub(-1, 1)
    excpected = -2
    assert  res == excpected, f"-1 - 1 should equal {excpected} not {res}"

