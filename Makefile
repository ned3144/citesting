PY := python3.8
PYTEST := pytest
TESTS := $(wildcard tests/*.py)

test:
	@echo "Testing..."
	@$(PYTEST) -q $(TESTS)
